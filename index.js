// console.log ("Hello World");

// What are conditional Statements?
//Conditional statements allow us to control the flow of our program and it allow us to run statement / instruction based on the condition.

// [SECTION] if, else if and else statements

let numA = 0;

// if statements
// Executes a statement if a specified condition is true

/*
  Syntax:
  if(condition){
  codeblock (statement);

}


*/

if (numA < 0) {
  console.log("Hello");

}

// the resulf of the expression in the if statement must result to true, else it will not run the statement inside.

// For checking the value
console.log(numA < 0);


let city = "New York";

if (city === "New York") {
  console.log("Welcome to New York City!");
}

// else if clause

// executes a statement if previous condition are false and if the specified condition is true;
// - the "else if" clause is optional and ca be added to caputre additional conditions to change the flow of a program.


let numH = 0;
if (numH < 0) {
  console.log("Hello")
} else if (numH > 0) {
  console.log("World");
}

city = "Manila";

if (city === "New York") {
  console.log("Welcome to New York City");
} else if (city === "Tokyo") {
  console.log("Welcome to Tokyo, Japan");
} else {
  console.log('Again');
}


// if, else if, and else statement inside a function

/*
Scenario: We want to determine intensity of a typhone based on its wind speed.
		Not a Typoon - Wind speed is less than 30.
		Tropical Depression - Wind speed is greater than or equal to 61.
		Tropical Storm - Wind speed is between 62 to 88.
		Severe Tropical Storm - Wind speed is between 89 to 117.
		Typoon - Wind speed is greater than or equal to 118.

	*/

let message = "No message";
console.log(message);

function determineTyphooneIntensity(windSpeed) {
  if (windSpeed < 30) {
    return "not a Typhoon yet."
  } else if (windSpeed <= 61) {
    return "Tropical depression Detected"
  } else if (windSpeed >= 62 && windSpeed <= 88) {
    return "tropical Storm Detected."
  } else if (windSpeed >= 89 && windSpeed <= 117) {
    return "Severe Tropical Storm."
  } else {
    return "Typhoon detected."
  }
}

message = determineTyphooneIntensity(110)
console.log(message);

// console.warn() is a good way to pring warnings in our console that could help us developers act on certain output.
if (message === "Sever Tropical storm detected");
console.warn(message);


/*[SECTION] Truthy and Falsy
  In JavaScript a "truthy" value is a value that is considered true when encountered in a boolean context
  -Falsy value/exception for truthy:
  1. false
  2. 0
  3.""
  4.null
  5.undefined
  6.NaN



*/

// Truthy Examples:
if (true) {
  console.log("Truthy");
}
if (1) {
  console.log("Truthy");
}
if ([]) {
  console.log("Truthy");
}
// Falsy

if (false) {
  console.log("Falsy");
}
if (0) {
  console.log("Falsy");
}
if (undefined) {
  console.log("Falsy");
}

// [SECTION] Conditional (ternary) operators

/*
  -The Conditional (ternary) operator takes in three operands:
  1. conditions
  2. expression to execute if the value is truthy.
  3. expression to execute if the value is falsy.
  -alternative for an "if else" statement
  -Ternary operators have an "Implicit return" statement meaning that without the "return" keyword. the resulting expressions can be stored in a variable.
  - Commonly used for single statement execution where the result consists of only one line of code.

*/

/*

Syntax:
  (expression) ? ifTrue : ifFalse;

  ? (answer if true)
  : (answer if false)


*/

let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator : " + ternaryResult);


// Multiple statement execution using ternary operators

let name;

function isOfLegalAge() {
  name = "John";
  return "You are of legal age limit.";
}

function isUnderAge() {
  name = "jane";
  return " You are under the age limit.";
}


// parseInt - converts the input received into a number data type.
//  if the input is note a number, it will return NaN (Not a Number)
// let age = parseInt(prompt("What is your age?"));
// console.log(age);
//
// let legalAge = (age >= 18 ) ? isOfLegalAge() : isUnderAge();
//   console.log("Result of Ternary Operator in Fucntions: " + legalAge + ", " +name);

/*
Nested if :
if(expression){
  if(expression){
    //code block
  }
}
  else {
    //code block
  }
  else {
  //code block
}


*/

//  [SECTION] Switch Statement

/*

  - The switch statement evaluates an expression and matches the expression's value to a case clause.


  -Syntax:
    switch (expression) {
      case value:
        statement/codeblock;
        break;
      default: <Just like else  (wala kang no choice.)
        satement/codeblock;
        break;
      }

*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
  case "monday":
    console.log("The color of the day is red.");
    break;
  case "tuesday":
    console.log("The color of the day is orange.");
    break;
  case "wednesday":
    console.log("The color of the day is yellow.");
    break;
  case "thursday":
    console.log("The color of the day is green.");
    break;
  case "friday":
    console.log("The color of the day is blue.");
    break;
  case "saturday":
    console.log("The color of the day is indigo.");
    break;
  case "sunday":
    console.log("The color of the day is violet.");
    break;
  default:
    console.log("Please input a valid day");
    break;
}


// [SECTION] Try-Catch-Finaly statements

/*
    -"Try catch" statements are commonly used for error handling.
    -It is also useful for debugging code because of the "error" object that can be caught when using the try catch statement.

    Syntax:
    try{
    // codeblock that we try to execute
  }
  // error/err are commonly used variable names for storing errors
  catch(error/err)
  finally {
  // continue execution of code regardless of the success and failure of the code execution
}


*/

function showIntensityAltert(windSpeed) {
  try {

    alerat(determineTyphooneIntensity(windSpeed));
  }
  catch (error){
    // error.message is used to access the information relating to an error object.
    console.warn(error);
  }
  finally {
    alert("Intensity updates will show new alert.")
  }
}

showIntensityAltert(56);
console.log("Hello World Again");





//









//
